<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html lang="fr-fr"><head><meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" >
<meta http-equiv="Content-Language" content="fr" >
<meta name="copyright" content="Copyright (c) 2005-07 Imaweb" >
<meta name="author" content="Net15/Eriidan" >
<meta name="version" content="1.1.3" >
<meta name="description" content="Le site de la Commune de Preaux en Seine Maritime" >
<meta name="keywords" content="preaux, pr&eacute;aux, preaux en normandie, preaux en seine maritime, mairie de preaux, commune de preaux" >
<link rel="top" type="text/html" href="http://www.preaux76.fr/public/" title="Accueil" >
<link rel="author" type="text/html" href="http://www.eriidan.fr/" title="Auteur" >
<link rel="copyright" type="text/html" href="http://www.net15.fr/" title="Droits" >
<link rel="stylesheet" type="text/css" href="http://www.preaux76.fr/public/style_browser.php" title="Style" >
<title>Préaux - Seine Maritime ~ Gestion des fichiers</title>
<script type="text/javascript" src="http://www.preaux76.fr/lib/overlib/overlib.js"><!-- overLIB (c) Erik Bosrup --></script>
<script type="text/javascript" src="http://www.preaux76.fr/lib/overlib/anchor.js"></script>
<script type="text/javascript" src="http://www.preaux76.fr/lib/overlib/centerpopup.js"></script>
<script type="text/javascript" src="http://www.preaux76.fr/lib/overlib/crossframe.js"></script>
<script type="text/javascript" src="http://www.preaux76.fr/lib/overlib/cssstyle.js"></script>
<script type="text/javascript" src="http://www.preaux76.fr/lib/overlib/debug.js"></script>
<script type="text/javascript" src="http://www.preaux76.fr/lib/overlib/exclusive.js"></script>
<script type="text/javascript" src="http://www.preaux76.fr/lib/overlib/followscroll.js"></script>
<script type="text/javascript" src="http://www.preaux76.fr/lib/overlib/hideform.js"></script>
<script type="text/javascript" src="http://www.preaux76.fr/lib/overlib/setonoff.js"></script>
<script type="text/javascript" src="http://www.preaux76.fr/lib/overlib/shadow.js"></script>
<script type="text/javascript" src="http://www.preaux76.fr/public/menu.js"></script>
<script type="text/javascript" src="http://www.preaux76.fr/public/menu.php"></script>
<script type="text/javascript" src="http://www.preaux76.fr/lib/func.js"></script>
</head>
<body><script type="text/javascript"><!--
overlib_pagedefaults(CENTER,WIDTH,380,OFFSETY,-52,FGCOLOR,'#ffffff',BGCOLOR,'#aeaeae',TEXTCOLOR,'#000000',TEXTSIZE,'9pt',CAPCOLOR,'#ffffff',CAPTIONSIZE,'7pt',BORDER,1);
--></script>
<div id="overDiv" style="position: absolute; visibility: hidden; z-index: 1000;"></div>
<script type="text/javascript">LoadMenus();</script>
<center><script type="text/javascript"><!-- 
function OpenFile(fileUrl){window.top.opener.SetUrl(fileUrl);window.top.close();window.top.opener.focus();}
function DisplayFile(fileUrl){window.open(fileUrl,'_blank','location=yes,menubar=no,status=yes');}
// -->
</script>
<h2>Gestion des fichiers</h2>
<p style="border-bottom: 1px #aeaeae solid">Dossier courant : la racine<br ><br ></p>
<p><table class="table_list"><tr><th class="cell_header" colspan="2" align="left">Nom</th>
<th class="cell_header" colspan="3" align="center">Action</th>
</tr>
<tr><td class="cell_normal_0" align="center"><img src="img/dossier.gif" alt="Dossier" ></td>
<td class="cell_normal_0"><a href="browser.php?page=browser&amp;standalone=&amp;Type=File" title="Sélectionner">File</a></td>
<td class="cell_normal_0" align="right" style="border-left: 1px #000000 solid">&nbsp;</td>
<td class="cell_normal_0" align="center">&nbsp;</td>
<td class="cell_normal_0" align="left">&nbsp;</td>
</tr>
<tr><td class="cell_normal_1" align="center"><img src="img/dossier.gif" alt="Dossier" ></td>
<td class="cell_normal_1"><a href="browser.php?page=browser&amp;standalone=&amp;Type=Flash" title="Sélectionner">Flash</a></td>
<td class="cell_normal_1" align="right" style="border-left: 1px #000000 solid">&nbsp;</td>
<td class="cell_normal_1" align="center">&nbsp;</td>
<td class="cell_normal_1" align="left">&nbsp;</td>
</tr>
<tr><td class="cell_normal_0" align="center"><img src="img/dossier.gif" alt="Dossier" ></td>
<td class="cell_normal_0"><a href="browser.php?page=browser&amp;standalone=&amp;Type=Image" title="Sélectionner">Image</a></td>
<td class="cell_normal_0" align="right" style="border-left: 1px #000000 solid">&nbsp;</td>
<td class="cell_normal_0" align="center">&nbsp;</td>
<td class="cell_normal_0" align="left">&nbsp;</td>
</tr>
<tr><td class="cell_normal_1" align="center"><img src="img/fichier.gif" alt="Fichier" ></td>
<td class="cell_normal_1"><a href="#" title="Sélectionner" onclick="javascript: OpenFile('http://www.preaux76.fr/UserFiles/Actu(1).jpg')">Actu(1).jpg</a></td>
<td class="cell_normal_1" align="right" style="border-left: 1px #000000 solid"><a href="#" title="Sélectionner" onclick="javascript: OpenFile('http://www.preaux76.fr/UserFiles/Actu(1).jpg')"><img src="img/select.gif" alt="Sélectionner" border="0"></a></td>
<td class="cell_normal_1" align="center"><a href="#" title="Voir" onclick="javascript: DisplayFile('http://www.preaux76.fr/UserFiles/Actu(1).jpg')"><img src="img/loupe.gif" alt="Voir" border="0"></a></td>
<td class="cell_normal_1" align="left"><a href="browser.php?page=browser&amp;standalone=&amp;a=delete_prop&amp;file_name=Actu(1).jpg" title="Supprimer"><img src="img/del.gif" alt="Supprimer" border="0"></a></td>
</tr>
<tr><td class="cell_normal_0" align="center"><img src="img/fichier.gif" alt="Fichier" ></td>
<td class="cell_normal_0"><a href="#" title="Sélectionner" onclick="javascript: OpenFile('http://www.preaux76.fr/UserFiles/Actu.jpg')">Actu.jpg</a></td>
<td class="cell_normal_0" align="right" style="border-left: 1px #000000 solid"><a href="#" title="Sélectionner" onclick="javascript: OpenFile('http://www.preaux76.fr/UserFiles/Actu.jpg')"><img src="img/select.gif" alt="Sélectionner" border="0"></a></td>
<td class="cell_normal_0" align="center"><a href="#" title="Voir" onclick="javascript: DisplayFile('http://www.preaux76.fr/UserFiles/Actu.jpg')"><img src="img/loupe.gif" alt="Voir" border="0"></a></td>
<td class="cell_normal_0" align="left"><a href="browser.php?page=browser&amp;standalone=&amp;a=delete_prop&amp;file_name=Actu.jpg" title="Supprimer"><img src="img/del.gif" alt="Supprimer" border="0"></a></td>
</tr>
<tr><td class="cell_normal_1" align="center"><img src="img/fichier.gif" alt="Fichier" ></td>
<td class="cell_normal_1"><a href="#" title="Sélectionner" onclick="javascript: OpenFile('http://www.preaux76.fr/UserFiles/CMJ.jpg')">CMJ.jpg</a></td>
<td class="cell_normal_1" align="right" style="border-left: 1px #000000 solid"><a href="#" title="Sélectionner" onclick="javascript: OpenFile('http://www.preaux76.fr/UserFiles/CMJ.jpg')"><img src="img/select.gif" alt="Sélectionner" border="0"></a></td>
<td class="cell_normal_1" align="center"><a href="#" title="Voir" onclick="javascript: DisplayFile('http://www.preaux76.fr/UserFiles/CMJ.jpg')"><img src="img/loupe.gif" alt="Voir" border="0"></a></td>
<td class="cell_normal_1" align="left"><a href="browser.php?page=browser&amp;standalone=&amp;a=delete_prop&amp;file_name=CMJ.jpg" title="Supprimer"><img src="img/del.gif" alt="Supprimer" border="0"></a></td>
</tr>
<tr><td class="cell_normal_0" align="center"><img src="img/fichier.gif" alt="Fichier" ></td>
<td class="cell_normal_0"><a href="#" title="Sélectionner" onclick="javascript: OpenFile('http://www.preaux76.fr/UserFiles/Circuits.pdf')">Circuits.pdf</a></td>
<td class="cell_normal_0" align="right" style="border-left: 1px #000000 solid"><a href="#" title="Sélectionner" onclick="javascript: OpenFile('http://www.preaux76.fr/UserFiles/Circuits.pdf')"><img src="img/select.gif" alt="Sélectionner" border="0"></a></td>
<td class="cell_normal_0" align="center"><a href="#" title="Voir" onclick="javascript: DisplayFile('http://www.preaux76.fr/UserFiles/Circuits.pdf')"><img src="img/loupe.gif" alt="Voir" border="0"></a></td>
<td class="cell_normal_0" align="left"><a href="browser.php?page=browser&amp;standalone=&amp;a=delete_prop&amp;file_name=Circuits.pdf" title="Supprimer"><img src="img/del.gif" alt="Supprimer" border="0"></a></td>
</tr>
<tr><td class="cell_normal_1" align="center"><img src="img/fichier.gif" alt="Fichier" ></td>
<td class="cell_normal_1"><a href="#" title="Sélectionner" onclick="javascript: OpenFile('http://www.preaux76.fr/UserFiles/Commissions.pdf')">Commissions.pdf</a></td>
<td class="cell_normal_1" align="right" style="border-left: 1px #000000 solid"><a href="#" title="Sélectionner" onclick="javascript: OpenFile('http://www.preaux76.fr/UserFiles/Commissions.pdf')"><img src="img/select.gif" alt="Sélectionner" border="0"></a></td>
<td class="cell_normal_1" align="center"><a href="#" title="Voir" onclick="javascript: DisplayFile('http://www.preaux76.fr/UserFiles/Commissions.pdf')"><img src="img/loupe.gif" alt="Voir" border="0"></a></td>
<td class="cell_normal_1" align="left"><a href="browser.php?page=browser&amp;standalone=&amp;a=delete_prop&amp;file_name=Commissions.pdf" title="Supprimer"><img src="img/del.gif" alt="Supprimer" border="0"></a></td>
</tr>
<tr><td class="cell_normal_0" align="center"><img src="img/fichier.gif" alt="Fichier" ></td>
<td class="cell_normal_0"><a href="#" title="Sélectionner" onclick="javascript: OpenFile('http://www.preaux76.fr/UserFiles/Compte rendu du Conseil Municipal du 13 mai 2009.pdf')">Compte rendu du Conseil Municipal du 13 mai 2009.pdf</a></td>
<td class="cell_normal_0" align="right" style="border-left: 1px #000000 solid"><a href="#" title="Sélectionner" onclick="javascript: OpenFile('http://www.preaux76.fr/UserFiles/Compte rendu du Conseil Municipal du 13 mai 2009.pdf')"><img src="img/select.gif" alt="Sélectionner" border="0"></a></td>
<td class="cell_normal_0" align="center"><a href="#" title="Voir" onclick="javascript: DisplayFile('http://www.preaux76.fr/UserFiles/Compte rendu du Conseil Municipal du 13 mai 2009.pdf')"><img src="img/loupe.gif" alt="Voir" border="0"></a></td>
<td class="cell_normal_0" align="left"><a href="browser.php?page=browser&amp;standalone=&amp;a=delete_prop&amp;file_name=Compte rendu du Conseil Municipal du 13 mai 2009.pdf" title="Supprimer"><img src="img/del.gif" alt="Supprimer" border="0"></a></td>
</tr>
<tr><td class="cell_normal_1" align="center"><img src="img/fichier.gif" alt="Fichier" ></td>
<td class="cell_normal_1"><a href="#" title="Sélectionner" onclick="javascript: OpenFile('http://www.preaux76.fr/UserFiles/Conseil Municipal_photo_090417.pdf')">Conseil Municipal_photo_090417.pdf</a></td>
<td class="cell_normal_1" align="right" style="border-left: 1px #000000 solid"><a href="#" title="Sélectionner" onclick="javascript: OpenFile('http://www.preaux76.fr/UserFiles/Conseil Municipal_photo_090417.pdf')"><img src="img/select.gif" alt="Sélectionner" border="0"></a></td>
<td class="cell_normal_1" align="center"><a href="#" title="Voir" onclick="javascript: DisplayFile('http://www.preaux76.fr/UserFiles/Conseil Municipal_photo_090417.pdf')"><img src="img/loupe.gif" alt="Voir" border="0"></a></td>
<td class="cell_normal_1" align="left"><a href="browser.php?page=browser&amp;standalone=&amp;a=delete_prop&amp;file_name=Conseil Municipal_photo_090417.pdf" title="Supprimer"><img src="img/del.gif" alt="Supprimer" border="0"></a></td>
</tr>
<tr><td class="cell_normal_0" align="center"><img src="img/fichier.gif" alt="Fichier" ></td>
<td class="cell_normal_0"><a href="#" title="Sélectionner" onclick="javascript: OpenFile('http://www.preaux76.fr/UserFiles/Cr_cm_090326.pdf')">Cr_cm_090326.pdf</a></td>
<td class="cell_normal_0" align="right" style="border-left: 1px #000000 solid"><a href="#" title="Sélectionner" onclick="javascript: OpenFile('http://www.preaux76.fr/UserFiles/Cr_cm_090326.pdf')"><img src="img/select.gif" alt="Sélectionner" border="0"></a></td>
<td class="cell_normal_0" align="center"><a href="#" title="Voir" onclick="javascript: DisplayFile('http://www.preaux76.fr/UserFiles/Cr_cm_090326.pdf')"><img src="img/loupe.gif" alt="Voir" border="0"></a></td>
<td class="cell_normal_0" align="left"><a href="browser.php?page=browser&amp;standalone=&amp;a=delete_prop&amp;file_name=Cr_cm_090326.pdf" title="Supprimer"><img src="img/del.gif" alt="Supprimer" border="0"></a></td>
</tr>
<tr><td class="cell_normal_1" align="center"><img src="img/fichier.gif" alt="Fichier" ></td>
<td class="cell_normal_1"><a href="#" title="Sélectionner" onclick="javascript: OpenFile('http://www.preaux76.fr/UserFiles/Dalle.JPG')">Dalle.JPG</a></td>
<td class="cell_normal_1" align="right" style="border-left: 1px #000000 solid"><a href="#" title="Sélectionner" onclick="javascript: OpenFile('http://www.preaux76.fr/UserFiles/Dalle.JPG')"><img src="img/select.gif" alt="Sélectionner" border="0"></a></td>
<td class="cell_normal_1" align="center"><a href="#" title="Voir" onclick="javascript: DisplayFile('http://www.preaux76.fr/UserFiles/Dalle.JPG')"><img src="img/loupe.gif" alt="Voir" border="0"></a></td>
<td class="cell_normal_1" align="left"><a href="browser.php?page=browser&amp;standalone=&amp;a=delete_prop&amp;file_name=Dalle.JPG" title="Supprimer"><img src="img/del.gif" alt="Supprimer" border="0"></a></td>
</tr>
<tr><td class="cell_normal_0" align="center"><img src="img/fichier.gif" alt="Fichier" ></td>
<td class="cell_normal_0"><a href="#" title="Sélectionner" onclick="javascript: OpenFile('http://www.preaux76.fr/UserFiles/Fiche_sanitaire_de_liaison_090410.pdf')">Fiche_sanitaire_de_liaison_090410.pdf</a></td>
<td class="cell_normal_0" align="right" style="border-left: 1px #000000 solid"><a href="#" title="Sélectionner" onclick="javascript: OpenFile('http://www.preaux76.fr/UserFiles/Fiche_sanitaire_de_liaison_090410.pdf')"><img src="img/select.gif" alt="Sélectionner" border="0"></a></td>
<td class="cell_normal_0" align="center"><a href="#" title="Voir" onclick="javascript: DisplayFile('http://www.preaux76.fr/UserFiles/Fiche_sanitaire_de_liaison_090410.pdf')"><img src="img/loupe.gif" alt="Voir" border="0"></a></td>
<td class="cell_normal_0" align="left"><a href="browser.php?page=browser&amp;standalone=&amp;a=delete_prop&amp;file_name=Fiche_sanitaire_de_liaison_090410.pdf" title="Supprimer"><img src="img/del.gif" alt="Supprimer" border="0"></a></td>
</tr>
<tr><td class="cell_normal_1" align="center"><img src="img/fichier.gif" alt="Fichier" ></td>
<td class="cell_normal_1"><a href="#" title="Sélectionner" onclick="javascript: OpenFile('http://www.preaux76.fr/UserFiles/Info_villages_recre_ete_2009_090410.pdf')">Info_villages_recre_ete_2009_090410.pdf</a></td>
<td class="cell_normal_1" align="right" style="border-left: 1px #000000 solid"><a href="#" title="Sélectionner" onclick="javascript: OpenFile('http://www.preaux76.fr/UserFiles/Info_villages_recre_ete_2009_090410.pdf')"><img src="img/select.gif" alt="Sélectionner" border="0"></a></td>
<td class="cell_normal_1" align="center"><a href="#" title="Voir" onclick="javascript: DisplayFile('http://www.preaux76.fr/UserFiles/Info_villages_recre_ete_2009_090410.pdf')"><img src="img/loupe.gif" alt="Voir" border="0"></a></td>
<td class="cell_normal_1" align="left"><a href="browser.php?page=browser&amp;standalone=&amp;a=delete_prop&amp;file_name=Info_villages_recre_ete_2009_090410.pdf" title="Supprimer"><img src="img/del.gif" alt="Supprimer" border="0"></a></td>
</tr>
<tr><td class="cell_normal_0" align="center"><img src="img/fichier.gif" alt="Fichier" ></td>
<td class="cell_normal_0"><a href="#" title="Sélectionner" onclick="javascript: OpenFile('http://www.preaux76.fr/UserFiles/PeB-02-2009.pdf')">PeB-02-2009.pdf</a></td>
<td class="cell_normal_0" align="right" style="border-left: 1px #000000 solid"><a href="#" title="Sélectionner" onclick="javascript: OpenFile('http://www.preaux76.fr/UserFiles/PeB-02-2009.pdf')"><img src="img/select.gif" alt="Sélectionner" border="0"></a></td>
<td class="cell_normal_0" align="center"><a href="#" title="Voir" onclick="javascript: DisplayFile('http://www.preaux76.fr/UserFiles/PeB-02-2009.pdf')"><img src="img/loupe.gif" alt="Voir" border="0"></a></td>
<td class="cell_normal_0" align="left"><a href="browser.php?page=browser&amp;standalone=&amp;a=delete_prop&amp;file_name=PeB-02-2009.pdf" title="Supprimer"><img src="img/del.gif" alt="Supprimer" border="0"></a></td>
</tr>
<tr><td class="cell_normal_1" align="center"><img src="img/fichier.gif" alt="Fichier" ></td>
<td class="cell_normal_1"><a href="#" title="Sélectionner" onclick="javascript: OpenFile('http://www.preaux76.fr/UserFiles/PeB-04-2009.pdf')">PeB-04-2009.pdf</a></td>
<td class="cell_normal_1" align="right" style="border-left: 1px #000000 solid"><a href="#" title="Sélectionner" onclick="javascript: OpenFile('http://www.preaux76.fr/UserFiles/PeB-04-2009.pdf')"><img src="img/select.gif" alt="Sélectionner" border="0"></a></td>
<td class="cell_normal_1" align="center"><a href="#" title="Voir" onclick="javascript: DisplayFile('http://www.preaux76.fr/UserFiles/PeB-04-2009.pdf')"><img src="img/loupe.gif" alt="Voir" border="0"></a></td>
<td class="cell_normal_1" align="left"><a href="browser.php?page=browser&amp;standalone=&amp;a=delete_prop&amp;file_name=PeB-04-2009.pdf" title="Supprimer"><img src="img/del.gif" alt="Supprimer" border="0"></a></td>
</tr>
<tr><td class="cell_normal_0" align="center"><img src="img/fichier.gif" alt="Fichier" ></td>
<td class="cell_normal_0"><a href="#" title="Sélectionner" onclick="javascript: OpenFile('http://www.preaux76.fr/UserFiles/Plan centre.pdf')">Plan centre.pdf</a></td>
<td class="cell_normal_0" align="right" style="border-left: 1px #000000 solid"><a href="#" title="Sélectionner" onclick="javascript: OpenFile('http://www.preaux76.fr/UserFiles/Plan centre.pdf')"><img src="img/select.gif" alt="Sélectionner" border="0"></a></td>
<td class="cell_normal_0" align="center"><a href="#" title="Voir" onclick="javascript: DisplayFile('http://www.preaux76.fr/UserFiles/Plan centre.pdf')"><img src="img/loupe.gif" alt="Voir" border="0"></a></td>
<td class="cell_normal_0" align="left"><a href="browser.php?page=browser&amp;standalone=&amp;a=delete_prop&amp;file_name=Plan centre.pdf" title="Supprimer"><img src="img/del.gif" alt="Supprimer" border="0"></a></td>
</tr>
<tr><td class="cell_normal_1" align="center"><img src="img/fichier.gif" alt="Fichier" ></td>
<td class="cell_normal_1"><a href="#" title="Sélectionner" onclick="javascript: OpenFile('http://www.preaux76.fr/UserFiles/Plan.jpg')">Plan.jpg</a></td>
<td class="cell_normal_1" align="right" style="border-left: 1px #000000 solid"><a href="#" title="Sélectionner" onclick="javascript: OpenFile('http://www.preaux76.fr/UserFiles/Plan.jpg')"><img src="img/select.gif" alt="Sélectionner" border="0"></a></td>
<td class="cell_normal_1" align="center"><a href="#" title="Voir" onclick="javascript: DisplayFile('http://www.preaux76.fr/UserFiles/Plan.jpg')"><img src="img/loupe.gif" alt="Voir" border="0"></a></td>
<td class="cell_normal_1" align="left"><a href="browser.php?page=browser&amp;standalone=&amp;a=delete_prop&amp;file_name=Plan.jpg" title="Supprimer"><img src="img/del.gif" alt="Supprimer" border="0"></a></td>
</tr>
<tr><td class="cell_normal_0" align="center"><img src="img/fichier.gif" alt="Fichier" ></td>
<td class="cell_normal_0"><a href="#" title="Sélectionner" onclick="javascript: OpenFile('http://www.preaux76.fr/UserFiles/Plan.pdf')">Plan.pdf</a></td>
<td class="cell_normal_0" align="right" style="border-left: 1px #000000 solid"><a href="#" title="Sélectionner" onclick="javascript: OpenFile('http://www.preaux76.fr/UserFiles/Plan.pdf')"><img src="img/select.gif" alt="Sélectionner" border="0"></a></td>
<td class="cell_normal_0" align="center"><a href="#" title="Voir" onclick="javascript: DisplayFile('http://www.preaux76.fr/UserFiles/Plan.pdf')"><img src="img/loupe.gif" alt="Voir" border="0"></a></td>
<td class="cell_normal_0" align="left"><a href="browser.php?page=browser&amp;standalone=&amp;a=delete_prop&amp;file_name=Plan.pdf" title="Supprimer"><img src="img/del.gif" alt="Supprimer" border="0"></a></td>
</tr>
<tr><td class="cell_normal_1" align="center"><img src="img/fichier.gif" alt="Fichier" ></td>
<td class="cell_normal_1"><a href="#" title="Sélectionner" onclick="javascript: OpenFile('http://www.preaux76.fr/UserFiles/Programme_ticket_sport_paques_09_090410.pdf')">Programme_ticket_sport_paques_09_090410.pdf</a></td>
<td class="cell_normal_1" align="right" style="border-left: 1px #000000 solid"><a href="#" title="Sélectionner" onclick="javascript: OpenFile('http://www.preaux76.fr/UserFiles/Programme_ticket_sport_paques_09_090410.pdf')"><img src="img/select.gif" alt="Sélectionner" border="0"></a></td>
<td class="cell_normal_1" align="center"><a href="#" title="Voir" onclick="javascript: DisplayFile('http://www.preaux76.fr/UserFiles/Programme_ticket_sport_paques_09_090410.pdf')"><img src="img/loupe.gif" alt="Voir" border="0"></a></td>
<td class="cell_normal_1" align="left"><a href="browser.php?page=browser&amp;standalone=&amp;a=delete_prop&amp;file_name=Programme_ticket_sport_paques_09_090410.pdf" title="Supprimer"><img src="img/del.gif" alt="Supprimer" border="0"></a></td>
</tr>
<tr><td class="cell_normal_0" align="center"><img src="img/fichier.gif" alt="Fichier" ></td>
<td class="cell_normal_0"><a href="#" title="Sélectionner" onclick="javascript: OpenFile('http://www.preaux76.fr/UserFiles/RIS%20randonn%E9e%20CCPM[1].pdf')">RIS%20randonn%E9e%20CCPM[1].pdf</a></td>
<td class="cell_normal_0" align="right" style="border-left: 1px #000000 solid"><a href="#" title="Sélectionner" onclick="javascript: OpenFile('http://www.preaux76.fr/UserFiles/RIS%20randonn%E9e%20CCPM[1].pdf')"><img src="img/select.gif" alt="Sélectionner" border="0"></a></td>
<td class="cell_normal_0" align="center"><a href="#" title="Voir" onclick="javascript: DisplayFile('http://www.preaux76.fr/UserFiles/RIS%20randonn%E9e%20CCPM[1].pdf')"><img src="img/loupe.gif" alt="Voir" border="0"></a></td>
<td class="cell_normal_0" align="left"><a href="browser.php?page=browser&amp;standalone=&amp;a=delete_prop&amp;file_name=RIS%20randonn%E9e%20CCPM[1].pdf" title="Supprimer"><img src="img/del.gif" alt="Supprimer" border="0"></a></td>
</tr>
<tr><td class="cell_normal_1" align="center"><img src="img/fichier.gif" alt="Fichier" ></td>
<td class="cell_normal_1"><a href="#" title="Sélectionner" onclick="javascript: OpenFile('http://www.preaux76.fr/UserFiles/Situation.pdf')">Situation.pdf</a></td>
<td class="cell_normal_1" align="right" style="border-left: 1px #000000 solid"><a href="#" title="Sélectionner" onclick="javascript: OpenFile('http://www.preaux76.fr/UserFiles/Situation.pdf')"><img src="img/select.gif" alt="Sélectionner" border="0"></a></td>
<td class="cell_normal_1" align="center"><a href="#" title="Voir" onclick="javascript: DisplayFile('http://www.preaux76.fr/UserFiles/Situation.pdf')"><img src="img/loupe.gif" alt="Voir" border="0"></a></td>
<td class="cell_normal_1" align="left"><a href="browser.php?page=browser&amp;standalone=&amp;a=delete_prop&amp;file_name=Situation.pdf" title="Supprimer"><img src="img/del.gif" alt="Supprimer" border="0"></a></td>
</tr>
<tr><td class="cell_normal_0" align="center"><img src="img/fichier.gif" alt="Fichier" ></td>
<td class="cell_normal_0"><a href="#" title="Sélectionner" onclick="javascript: OpenFile('http://www.preaux76.fr/UserFiles/actu_mai_090602.pdf')">actu_mai_090602.pdf</a></td>
<td class="cell_normal_0" align="right" style="border-left: 1px #000000 solid"><a href="#" title="Sélectionner" onclick="javascript: OpenFile('http://www.preaux76.fr/UserFiles/actu_mai_090602.pdf')"><img src="img/select.gif" alt="Sélectionner" border="0"></a></td>
<td class="cell_normal_0" align="center"><a href="#" title="Voir" onclick="javascript: DisplayFile('http://www.preaux76.fr/UserFiles/actu_mai_090602.pdf')"><img src="img/loupe.gif" alt="Voir" border="0"></a></td>
<td class="cell_normal_0" align="left"><a href="browser.php?page=browser&amp;standalone=&amp;a=delete_prop&amp;file_name=actu_mai_090602.pdf" title="Supprimer"><img src="img/del.gif" alt="Supprimer" border="0"></a></td>
</tr>
<tr><td class="cell_normal_1" align="center"><img src="img/fichier.gif" alt="Fichier" ></td>
<td class="cell_normal_1"><a href="#" title="Sélectionner" onclick="javascript: OpenFile('http://www.preaux76.fr/UserFiles/biblio_presentation_090508.pdf')">biblio_presentation_090508.pdf</a></td>
<td class="cell_normal_1" align="right" style="border-left: 1px #000000 solid"><a href="#" title="Sélectionner" onclick="javascript: OpenFile('http://www.preaux76.fr/UserFiles/biblio_presentation_090508.pdf')"><img src="img/select.gif" alt="Sélectionner" border="0"></a></td>
<td class="cell_normal_1" align="center"><a href="#" title="Voir" onclick="javascript: DisplayFile('http://www.preaux76.fr/UserFiles/biblio_presentation_090508.pdf')"><img src="img/loupe.gif" alt="Voir" border="0"></a></td>
<td class="cell_normal_1" align="left"><a href="browser.php?page=browser&amp;standalone=&amp;a=delete_prop&amp;file_name=biblio_presentation_090508.pdf" title="Supprimer"><img src="img/del.gif" alt="Supprimer" border="0"></a></td>
</tr>
<tr><td class="cell_normal_0" align="center"><img src="img/fichier.gif" alt="Fichier" ></td>
<td class="cell_normal_0"><a href="#" title="Sélectionner" onclick="javascript: OpenFile('http://www.preaux76.fr/UserFiles/cidre_duval.BMP')">cidre_duval.BMP</a></td>
<td class="cell_normal_0" align="right" style="border-left: 1px #000000 solid"><a href="#" title="Sélectionner" onclick="javascript: OpenFile('http://www.preaux76.fr/UserFiles/cidre_duval.BMP')"><img src="img/select.gif" alt="Sélectionner" border="0"></a></td>
<td class="cell_normal_0" align="center"><a href="#" title="Voir" onclick="javascript: DisplayFile('http://www.preaux76.fr/UserFiles/cidre_duval.BMP')"><img src="img/loupe.gif" alt="Voir" border="0"></a></td>
<td class="cell_normal_0" align="left"><a href="browser.php?page=browser&amp;standalone=&amp;a=delete_prop&amp;file_name=cidre_duval.BMP" title="Supprimer"><img src="img/del.gif" alt="Supprimer" border="0"></a></td>
</tr>
<tr><td class="cell_normal_1" align="center"><img src="img/fichier.gif" alt="Fichier" ></td>
<td class="cell_normal_1"><a href="#" title="Sélectionner" onclick="javascript: OpenFile('http://www.preaux76.fr/UserFiles/conf_alzheimer_090429.pdf')">conf_alzheimer_090429.pdf</a></td>
<td class="cell_normal_1" align="right" style="border-left: 1px #000000 solid"><a href="#" title="Sélectionner" onclick="javascript: OpenFile('http://www.preaux76.fr/UserFiles/conf_alzheimer_090429.pdf')"><img src="img/select.gif" alt="Sélectionner" border="0"></a></td>
<td class="cell_normal_1" align="center"><a href="#" title="Voir" onclick="javascript: DisplayFile('http://www.preaux76.fr/UserFiles/conf_alzheimer_090429.pdf')"><img src="img/loupe.gif" alt="Voir" border="0"></a></td>
<td class="cell_normal_1" align="left"><a href="browser.php?page=browser&amp;standalone=&amp;a=delete_prop&amp;file_name=conf_alzheimer_090429.pdf" title="Supprimer"><img src="img/del.gif" alt="Supprimer" border="0"></a></td>
</tr>
<tr><td class="cell_normal_0" align="center"><img src="img/fichier.gif" alt="Fichier" ></td>
<td class="cell_normal_0"><a href="#" title="Sélectionner" onclick="javascript: OpenFile('http://www.preaux76.fr/UserFiles/dossier_inscription_CLSH_ete_2009_A4_090512.pdf')">dossier_inscription_CLSH_ete_2009_A4_090512.pdf</a></td>
<td class="cell_normal_0" align="right" style="border-left: 1px #000000 solid"><a href="#" title="Sélectionner" onclick="javascript: OpenFile('http://www.preaux76.fr/UserFiles/dossier_inscription_CLSH_ete_2009_A4_090512.pdf')"><img src="img/select.gif" alt="Sélectionner" border="0"></a></td>
<td class="cell_normal_0" align="center"><a href="#" title="Voir" onclick="javascript: DisplayFile('http://www.preaux76.fr/UserFiles/dossier_inscription_CLSH_ete_2009_A4_090512.pdf')"><img src="img/loupe.gif" alt="Voir" border="0"></a></td>
<td class="cell_normal_0" align="left"><a href="browser.php?page=browser&amp;standalone=&amp;a=delete_prop&amp;file_name=dossier_inscription_CLSH_ete_2009_A4_090512.pdf" title="Supprimer"><img src="img/del.gif" alt="Supprimer" border="0"></a></td>
</tr>
<tr><td class="cell_normal_1" align="center"><img src="img/fichier.gif" alt="Fichier" ></td>
<td class="cell_normal_1"><a href="#" title="Sélectionner" onclick="javascript: OpenFile('http://www.preaux76.fr/UserFiles/dossier_inscription_sejours_vacances_ete_09_090410.pdf')">dossier_inscription_sejours_vacances_ete_09_090410.pdf</a></td>
<td class="cell_normal_1" align="right" style="border-left: 1px #000000 solid"><a href="#" title="Sélectionner" onclick="javascript: OpenFile('http://www.preaux76.fr/UserFiles/dossier_inscription_sejours_vacances_ete_09_090410.pdf')"><img src="img/select.gif" alt="Sélectionner" border="0"></a></td>
<td class="cell_normal_1" align="center"><a href="#" title="Voir" onclick="javascript: DisplayFile('http://www.preaux76.fr/UserFiles/dossier_inscription_sejours_vacances_ete_09_090410.pdf')"><img src="img/loupe.gif" alt="Voir" border="0"></a></td>
<td class="cell_normal_1" align="left"><a href="browser.php?page=browser&amp;standalone=&amp;a=delete_prop&amp;file_name=dossier_inscription_sejours_vacances_ete_09_090410.pdf" title="Supprimer"><img src="img/del.gif" alt="Supprimer" border="0"></a></td>
</tr>
<tr><td class="cell_normal_0" align="center"><img src="img/fichier.gif" alt="Fichier" ></td>
<td class="cell_normal_0"><a href="#" title="Sélectionner" onclick="javascript: OpenFile('http://www.preaux76.fr/UserFiles/dossier_inscription_villages_recre_paques_09_090410.pdf')">dossier_inscription_villages_recre_paques_09_090410.pdf</a></td>
<td class="cell_normal_0" align="right" style="border-left: 1px #000000 solid"><a href="#" title="Sélectionner" onclick="javascript: OpenFile('http://www.preaux76.fr/UserFiles/dossier_inscription_villages_recre_paques_09_090410.pdf')"><img src="img/select.gif" alt="Sélectionner" border="0"></a></td>
<td class="cell_normal_0" align="center"><a href="#" title="Voir" onclick="javascript: DisplayFile('http://www.preaux76.fr/UserFiles/dossier_inscription_villages_recre_paques_09_090410.pdf')"><img src="img/loupe.gif" alt="Voir" border="0"></a></td>
<td class="cell_normal_0" align="left"><a href="browser.php?page=browser&amp;standalone=&amp;a=delete_prop&amp;file_name=dossier_inscription_villages_recre_paques_09_090410.pdf" title="Supprimer"><img src="img/del.gif" alt="Supprimer" border="0"></a></td>
</tr>
<tr><td class="cell_normal_1" align="center"><img src="img/fichier.gif" alt="Fichier" ></td>
<td class="cell_normal_1"><a href="#" title="Sélectionner" onclick="javascript: OpenFile('http://www.preaux76.fr/UserFiles/info_clsh_paques_2009_servaville_090410.pdf')">info_clsh_paques_2009_servaville_090410.pdf</a></td>
<td class="cell_normal_1" align="right" style="border-left: 1px #000000 solid"><a href="#" title="Sélectionner" onclick="javascript: OpenFile('http://www.preaux76.fr/UserFiles/info_clsh_paques_2009_servaville_090410.pdf')"><img src="img/select.gif" alt="Sélectionner" border="0"></a></td>
<td class="cell_normal_1" align="center"><a href="#" title="Voir" onclick="javascript: DisplayFile('http://www.preaux76.fr/UserFiles/info_clsh_paques_2009_servaville_090410.pdf')"><img src="img/loupe.gif" alt="Voir" border="0"></a></td>
<td class="cell_normal_1" align="left"><a href="browser.php?page=browser&amp;standalone=&amp;a=delete_prop&amp;file_name=info_clsh_paques_2009_servaville_090410.pdf" title="Supprimer"><img src="img/del.gif" alt="Supprimer" border="0"></a></td>
</tr>
<tr><td class="cell_normal_0" align="center"><img src="img/fichier.gif" alt="Fichier" ></td>
<td class="cell_normal_0"><a href="#" title="Sélectionner" onclick="javascript: OpenFile('http://www.preaux76.fr/UserFiles/lien_clic_090429.doc')">lien_clic_090429.doc</a></td>
<td class="cell_normal_0" align="right" style="border-left: 1px #000000 solid"><a href="#" title="Sélectionner" onclick="javascript: OpenFile('http://www.preaux76.fr/UserFiles/lien_clic_090429.doc')"><img src="img/select.gif" alt="Sélectionner" border="0"></a></td>
<td class="cell_normal_0" align="center"><a href="#" title="Voir" onclick="javascript: DisplayFile('http://www.preaux76.fr/UserFiles/lien_clic_090429.doc')"><img src="img/loupe.gif" alt="Voir" border="0"></a></td>
<td class="cell_normal_0" align="left"><a href="browser.php?page=browser&amp;standalone=&amp;a=delete_prop&amp;file_name=lien_clic_090429.doc" title="Supprimer"><img src="img/del.gif" alt="Supprimer" border="0"></a></td>
</tr>
<tr><td class="cell_normal_1" align="center"><img src="img/fichier.gif" alt="Fichier" ></td>
<td class="cell_normal_1"><a href="#" title="Sélectionner" onclick="javascript: OpenFile('http://www.preaux76.fr/UserFiles/menu_elementaire_11_mai_12_06_09.pdf')">menu_elementaire_11_mai_12_06_09.pdf</a></td>
<td class="cell_normal_1" align="right" style="border-left: 1px #000000 solid"><a href="#" title="Sélectionner" onclick="javascript: OpenFile('http://www.preaux76.fr/UserFiles/menu_elementaire_11_mai_12_06_09.pdf')"><img src="img/select.gif" alt="Sélectionner" border="0"></a></td>
<td class="cell_normal_1" align="center"><a href="#" title="Voir" onclick="javascript: DisplayFile('http://www.preaux76.fr/UserFiles/menu_elementaire_11_mai_12_06_09.pdf')"><img src="img/loupe.gif" alt="Voir" border="0"></a></td>
<td class="cell_normal_1" align="left"><a href="browser.php?page=browser&amp;standalone=&amp;a=delete_prop&amp;file_name=menu_elementaire_11_mai_12_06_09.pdf" title="Supprimer"><img src="img/del.gif" alt="Supprimer" border="0"></a></td>
</tr>
<tr><td class="cell_normal_0" align="center"><img src="img/fichier.gif" alt="Fichier" ></td>
<td class="cell_normal_0"><a href="#" title="Sélectionner" onclick="javascript: OpenFile('http://www.preaux76.fr/UserFiles/menu_maternelle_11_mai_120609.pdf')">menu_maternelle_11_mai_120609.pdf</a></td>
<td class="cell_normal_0" align="right" style="border-left: 1px #000000 solid"><a href="#" title="Sélectionner" onclick="javascript: OpenFile('http://www.preaux76.fr/UserFiles/menu_maternelle_11_mai_120609.pdf')"><img src="img/select.gif" alt="Sélectionner" border="0"></a></td>
<td class="cell_normal_0" align="center"><a href="#" title="Voir" onclick="javascript: DisplayFile('http://www.preaux76.fr/UserFiles/menu_maternelle_11_mai_120609.pdf')"><img src="img/loupe.gif" alt="Voir" border="0"></a></td>
<td class="cell_normal_0" align="left"><a href="browser.php?page=browser&amp;standalone=&amp;a=delete_prop&amp;file_name=menu_maternelle_11_mai_120609.pdf" title="Supprimer"><img src="img/del.gif" alt="Supprimer" border="0"></a></td>
</tr>
<tr><td class="cell_normal_1" align="center"><img src="img/fichier.gif" alt="Fichier" ></td>
<td class="cell_normal_1"><a href="#" title="Sélectionner" onclick="javascript: OpenFile('http://www.preaux76.fr/UserFiles/pdflogo.jpg')">pdflogo.jpg</a></td>
<td class="cell_normal_1" align="right" style="border-left: 1px #000000 solid"><a href="#" title="Sélectionner" onclick="javascript: OpenFile('http://www.preaux76.fr/UserFiles/pdflogo.jpg')"><img src="img/select.gif" alt="Sélectionner" border="0"></a></td>
<td class="cell_normal_1" align="center"><a href="#" title="Voir" onclick="javascript: DisplayFile('http://www.preaux76.fr/UserFiles/pdflogo.jpg')"><img src="img/loupe.gif" alt="Voir" border="0"></a></td>
<td class="cell_normal_1" align="left"><a href="browser.php?page=browser&amp;standalone=&amp;a=delete_prop&amp;file_name=pdflogo.jpg" title="Supprimer"><img src="img/del.gif" alt="Supprimer" border="0"></a></td>
</tr>
<tr><td class="cell_normal_0" align="center"><img src="img/fichier.gif" alt="Fichier" ></td>
<td class="cell_normal_0"><a href="#" title="Sélectionner" onclick="javascript: OpenFile('http://www.preaux76.fr/UserFiles/photo-test.jpg')">photo-test.jpg</a></td>
<td class="cell_normal_0" align="right" style="border-left: 1px #000000 solid"><a href="#" title="Sélectionner" onclick="javascript: OpenFile('http://www.preaux76.fr/UserFiles/photo-test.jpg')"><img src="img/select.gif" alt="Sélectionner" border="0"></a></td>
<td class="cell_normal_0" align="center"><a href="#" title="Voir" onclick="javascript: DisplayFile('http://www.preaux76.fr/UserFiles/photo-test.jpg')"><img src="img/loupe.gif" alt="Voir" border="0"></a></td>
<td class="cell_normal_0" align="left"><a href="browser.php?page=browser&amp;standalone=&amp;a=delete_prop&amp;file_name=photo-test.jpg" title="Supprimer"><img src="img/del.gif" alt="Supprimer" border="0"></a></td>
</tr>
<tr><td class="cell_normal_1" align="center"><img src="img/fichier.gif" alt="Fichier" ></td>
<td class="cell_normal_1"><a href="#" title="Sélectionner" onclick="javascript: OpenFile('http://www.preaux76.fr/UserFiles/presentation_clsh_ete_ 2009.pdf')">presentation_clsh_ete_ 2009.pdf</a></td>
<td class="cell_normal_1" align="right" style="border-left: 1px #000000 solid"><a href="#" title="Sélectionner" onclick="javascript: OpenFile('http://www.preaux76.fr/UserFiles/presentation_clsh_ete_ 2009.pdf')"><img src="img/select.gif" alt="Sélectionner" border="0"></a></td>
<td class="cell_normal_1" align="center"><a href="#" title="Voir" onclick="javascript: DisplayFile('http://www.preaux76.fr/UserFiles/presentation_clsh_ete_ 2009.pdf')"><img src="img/loupe.gif" alt="Voir" border="0"></a></td>
<td class="cell_normal_1" align="left"><a href="browser.php?page=browser&amp;standalone=&amp;a=delete_prop&amp;file_name=presentation_clsh_ete_ 2009.pdf" title="Supprimer"><img src="img/del.gif" alt="Supprimer" border="0"></a></td>
</tr>
<tr><td class="cell_normal_0" align="center"><img src="img/fichier.gif" alt="Fichier" ></td>
<td class="cell_normal_0"><a href="#" title="Sélectionner" onclick="javascript: OpenFile('http://www.preaux76.fr/UserFiles/presentation_clsh_petites vacances_090319_090414.pdf')">presentation_clsh_petites vacances_090319_090414.pdf</a></td>
<td class="cell_normal_0" align="right" style="border-left: 1px #000000 solid"><a href="#" title="Sélectionner" onclick="javascript: OpenFile('http://www.preaux76.fr/UserFiles/presentation_clsh_petites vacances_090319_090414.pdf')"><img src="img/select.gif" alt="Sélectionner" border="0"></a></td>
<td class="cell_normal_0" align="center"><a href="#" title="Voir" onclick="javascript: DisplayFile('http://www.preaux76.fr/UserFiles/presentation_clsh_petites vacances_090319_090414.pdf')"><img src="img/loupe.gif" alt="Voir" border="0"></a></td>
<td class="cell_normal_0" align="left"><a href="browser.php?page=browser&amp;standalone=&amp;a=delete_prop&amp;file_name=presentation_clsh_petites vacances_090319_090414.pdf" title="Supprimer"><img src="img/del.gif" alt="Supprimer" border="0"></a></td>
</tr>
<tr><td class="cell_normal_1" align="center"><img src="img/fichier.gif" alt="Fichier" ></td>
<td class="cell_normal_1"><a href="#" title="Sélectionner" onclick="javascript: OpenFile('http://www.preaux76.fr/UserFiles/presentation_cmj_090319.doc')">presentation_cmj_090319.doc</a></td>
<td class="cell_normal_1" align="right" style="border-left: 1px #000000 solid"><a href="#" title="Sélectionner" onclick="javascript: OpenFile('http://www.preaux76.fr/UserFiles/presentation_cmj_090319.doc')"><img src="img/select.gif" alt="Sélectionner" border="0"></a></td>
<td class="cell_normal_1" align="center"><a href="#" title="Voir" onclick="javascript: DisplayFile('http://www.preaux76.fr/UserFiles/presentation_cmj_090319.doc')"><img src="img/loupe.gif" alt="Voir" border="0"></a></td>
<td class="cell_normal_1" align="left"><a href="browser.php?page=browser&amp;standalone=&amp;a=delete_prop&amp;file_name=presentation_cmj_090319.doc" title="Supprimer"><img src="img/del.gif" alt="Supprimer" border="0"></a></td>
</tr>
<tr><td class="cell_normal_0" align="center"><img src="img/fichier.gif" alt="Fichier" ></td>
<td class="cell_normal_0"><a href="#" title="Sélectionner" onclick="javascript: OpenFile('http://www.preaux76.fr/UserFiles/presentation_ticket_sport_090319.pdf')">presentation_ticket_sport_090319.pdf</a></td>
<td class="cell_normal_0" align="right" style="border-left: 1px #000000 solid"><a href="#" title="Sélectionner" onclick="javascript: OpenFile('http://www.preaux76.fr/UserFiles/presentation_ticket_sport_090319.pdf')"><img src="img/select.gif" alt="Sélectionner" border="0"></a></td>
<td class="cell_normal_0" align="center"><a href="#" title="Voir" onclick="javascript: DisplayFile('http://www.preaux76.fr/UserFiles/presentation_ticket_sport_090319.pdf')"><img src="img/loupe.gif" alt="Voir" border="0"></a></td>
<td class="cell_normal_0" align="left"><a href="browser.php?page=browser&amp;standalone=&amp;a=delete_prop&amp;file_name=presentation_ticket_sport_090319.pdf" title="Supprimer"><img src="img/del.gif" alt="Supprimer" border="0"></a></td>
</tr>
<tr><td class="cell_normal_1" align="center"><img src="img/fichier.gif" alt="Fichier" ></td>
<td class="cell_normal_1"><a href="#" title="Sélectionner" onclick="javascript: OpenFile('http://www.preaux76.fr/UserFiles/programme_villages_recre_ete_2009_090526.odt')">programme_villages_recre_ete_2009_090526.odt</a></td>
<td class="cell_normal_1" align="right" style="border-left: 1px #000000 solid"><a href="#" title="Sélectionner" onclick="javascript: OpenFile('http://www.preaux76.fr/UserFiles/programme_villages_recre_ete_2009_090526.odt')"><img src="img/select.gif" alt="Sélectionner" border="0"></a></td>
<td class="cell_normal_1" align="center"><a href="#" title="Voir" onclick="javascript: DisplayFile('http://www.preaux76.fr/UserFiles/programme_villages_recre_ete_2009_090526.odt')"><img src="img/loupe.gif" alt="Voir" border="0"></a></td>
<td class="cell_normal_1" align="left"><a href="browser.php?page=browser&amp;standalone=&amp;a=delete_prop&amp;file_name=programme_villages_recre_ete_2009_090526.odt" title="Supprimer"><img src="img/del.gif" alt="Supprimer" border="0"></a></td>
</tr>
<tr><td class="cell_normal_0" align="center"><img src="img/fichier.gif" alt="Fichier" ></td>
<td class="cell_normal_0"><a href="#" title="Sélectionner" onclick="javascript: OpenFile('http://www.preaux76.fr/UserFiles/prÃ©aux plan.JPG')">prÃ©aux plan.JPG</a></td>
<td class="cell_normal_0" align="right" style="border-left: 1px #000000 solid"><a href="#" title="Sélectionner" onclick="javascript: OpenFile('http://www.preaux76.fr/UserFiles/prÃ©aux plan.JPG')"><img src="img/select.gif" alt="Sélectionner" border="0"></a></td>
<td class="cell_normal_0" align="center"><a href="#" title="Voir" onclick="javascript: DisplayFile('http://www.preaux76.fr/UserFiles/prÃ©aux plan.JPG')"><img src="img/loupe.gif" alt="Voir" border="0"></a></td>
<td class="cell_normal_0" align="left"><a href="browser.php?page=browser&amp;standalone=&amp;a=delete_prop&amp;file_name=prÃ©aux plan.JPG" title="Supprimer"><img src="img/del.gif" alt="Supprimer" border="0"></a></td>
</tr>
<tr><td class="cell_normal_1" align="center"><img src="img/fichier.gif" alt="Fichier" ></td>
<td class="cell_normal_1"><a href="#" title="Sélectionner" onclick="javascript: OpenFile('http://www.preaux76.fr/UserFiles/rando-25_avril_090417.pdf')">rando-25_avril_090417.pdf</a></td>
<td class="cell_normal_1" align="right" style="border-left: 1px #000000 solid"><a href="#" title="Sélectionner" onclick="javascript: OpenFile('http://www.preaux76.fr/UserFiles/rando-25_avril_090417.pdf')"><img src="img/select.gif" alt="Sélectionner" border="0"></a></td>
<td class="cell_normal_1" align="center"><a href="#" title="Voir" onclick="javascript: DisplayFile('http://www.preaux76.fr/UserFiles/rando-25_avril_090417.pdf')"><img src="img/loupe.gif" alt="Voir" border="0"></a></td>
<td class="cell_normal_1" align="left"><a href="browser.php?page=browser&amp;standalone=&amp;a=delete_prop&amp;file_name=rando-25_avril_090417.pdf" title="Supprimer"><img src="img/del.gif" alt="Supprimer" border="0"></a></td>
</tr>
<tr><td class="cell_normal_0" align="center"><img src="img/fichier.gif" alt="Fichier" ></td>
<td class="cell_normal_0"><a href="#" title="Sélectionner" onclick="javascript: OpenFile('http://www.preaux76.fr/UserFiles/reglement_interieur_ete_clsh_sejours_2009_090410.pdf')">reglement_interieur_ete_clsh_sejours_2009_090410.pdf</a></td>
<td class="cell_normal_0" align="right" style="border-left: 1px #000000 solid"><a href="#" title="Sélectionner" onclick="javascript: OpenFile('http://www.preaux76.fr/UserFiles/reglement_interieur_ete_clsh_sejours_2009_090410.pdf')"><img src="img/select.gif" alt="Sélectionner" border="0"></a></td>
<td class="cell_normal_0" align="center"><a href="#" title="Voir" onclick="javascript: DisplayFile('http://www.preaux76.fr/UserFiles/reglement_interieur_ete_clsh_sejours_2009_090410.pdf')"><img src="img/loupe.gif" alt="Voir" border="0"></a></td>
<td class="cell_normal_0" align="left"><a href="browser.php?page=browser&amp;standalone=&amp;a=delete_prop&amp;file_name=reglement_interieur_ete_clsh_sejours_2009_090410.pdf" title="Supprimer"><img src="img/del.gif" alt="Supprimer" border="0"></a></td>
</tr>
<tr><td class="cell_normal_1" align="center"><img src="img/fichier.gif" alt="Fichier" ></td>
<td class="cell_normal_1"><a href="#" title="Sélectionner" onclick="javascript: OpenFile('http://www.preaux76.fr/UserFiles/reglement_interieur_petites_vacances_090512_1.pdf')">reglement_interieur_petites_vacances_090512_1.pdf</a></td>
<td class="cell_normal_1" align="right" style="border-left: 1px #000000 solid"><a href="#" title="Sélectionner" onclick="javascript: OpenFile('http://www.preaux76.fr/UserFiles/reglement_interieur_petites_vacances_090512_1.pdf')"><img src="img/select.gif" alt="Sélectionner" border="0"></a></td>
<td class="cell_normal_1" align="center"><a href="#" title="Voir" onclick="javascript: DisplayFile('http://www.preaux76.fr/UserFiles/reglement_interieur_petites_vacances_090512_1.pdf')"><img src="img/loupe.gif" alt="Voir" border="0"></a></td>
<td class="cell_normal_1" align="left"><a href="browser.php?page=browser&amp;standalone=&amp;a=delete_prop&amp;file_name=reglement_interieur_petites_vacances_090512_1.pdf" title="Supprimer"><img src="img/del.gif" alt="Supprimer" border="0"></a></td>
</tr>
<tr><td class="cell_normal_0" align="center"><img src="img/fichier.gif" alt="Fichier" ></td>
<td class="cell_normal_0"><a href="#" title="Sélectionner" onclick="javascript: OpenFile('http://www.preaux76.fr/UserFiles/reglement_intÃ©rieur_petites_vacances_090512.pdf')">reglement_intÃ©rieur_petites_vacances_090512.pdf</a></td>
<td class="cell_normal_0" align="right" style="border-left: 1px #000000 solid"><a href="#" title="Sélectionner" onclick="javascript: OpenFile('http://www.preaux76.fr/UserFiles/reglement_intÃ©rieur_petites_vacances_090512.pdf')"><img src="img/select.gif" alt="Sélectionner" border="0"></a></td>
<td class="cell_normal_0" align="center"><a href="#" title="Voir" onclick="javascript: DisplayFile('http://www.preaux76.fr/UserFiles/reglement_intÃ©rieur_petites_vacances_090512.pdf')"><img src="img/loupe.gif" alt="Voir" border="0"></a></td>
<td class="cell_normal_0" align="left"><a href="browser.php?page=browser&amp;standalone=&amp;a=delete_prop&amp;file_name=reglement_intÃ©rieur_petites_vacances_090512.pdf" title="Supprimer"><img src="img/del.gif" alt="Supprimer" border="0"></a></td>
</tr>
<tr><td class="cell_normal_1" align="center"><img src="img/fichier.gif" alt="Fichier" ></td>
<td class="cell_normal_1"><a href="#" title="Sélectionner" onclick="javascript: OpenFile('http://www.preaux76.fr/UserFiles/situation(1).jpg')">situation(1).jpg</a></td>
<td class="cell_normal_1" align="right" style="border-left: 1px #000000 solid"><a href="#" title="Sélectionner" onclick="javascript: OpenFile('http://www.preaux76.fr/UserFiles/situation(1).jpg')"><img src="img/select.gif" alt="Sélectionner" border="0"></a></td>
<td class="cell_normal_1" align="center"><a href="#" title="Voir" onclick="javascript: DisplayFile('http://www.preaux76.fr/UserFiles/situation(1).jpg')"><img src="img/loupe.gif" alt="Voir" border="0"></a></td>
<td class="cell_normal_1" align="left"><a href="browser.php?page=browser&amp;standalone=&amp;a=delete_prop&amp;file_name=situation(1).jpg" title="Supprimer"><img src="img/del.gif" alt="Supprimer" border="0"></a></td>
</tr>
<tr><td class="cell_normal_0" align="center"><img src="img/fichier.gif" alt="Fichier" ></td>
<td class="cell_normal_0"><a href="#" title="Sélectionner" onclick="javascript: OpenFile('http://www.preaux76.fr/UserFiles/situation(2).jpg')">situation(2).jpg</a></td>
<td class="cell_normal_0" align="right" style="border-left: 1px #000000 solid"><a href="#" title="Sélectionner" onclick="javascript: OpenFile('http://www.preaux76.fr/UserFiles/situation(2).jpg')"><img src="img/select.gif" alt="Sélectionner" border="0"></a></td>
<td class="cell_normal_0" align="center"><a href="#" title="Voir" onclick="javascript: DisplayFile('http://www.preaux76.fr/UserFiles/situation(2).jpg')"><img src="img/loupe.gif" alt="Voir" border="0"></a></td>
<td class="cell_normal_0" align="left"><a href="browser.php?page=browser&amp;standalone=&amp;a=delete_prop&amp;file_name=situation(2).jpg" title="Supprimer"><img src="img/del.gif" alt="Supprimer" border="0"></a></td>
</tr>
<tr><td class="cell_normal_1" align="center"><img src="img/fichier.gif" alt="Fichier" ></td>
<td class="cell_normal_1"><a href="#" title="Sélectionner" onclick="javascript: OpenFile('http://www.preaux76.fr/UserFiles/situation.jpg')">situation.jpg</a></td>
<td class="cell_normal_1" align="right" style="border-left: 1px #000000 solid"><a href="#" title="Sélectionner" onclick="javascript: OpenFile('http://www.preaux76.fr/UserFiles/situation.jpg')"><img src="img/select.gif" alt="Sélectionner" border="0"></a></td>
<td class="cell_normal_1" align="center"><a href="#" title="Voir" onclick="javascript: DisplayFile('http://www.preaux76.fr/UserFiles/situation.jpg')"><img src="img/loupe.gif" alt="Voir" border="0"></a></td>
<td class="cell_normal_1" align="left"><a href="browser.php?page=browser&amp;standalone=&amp;a=delete_prop&amp;file_name=situation.jpg" title="Supprimer"><img src="img/del.gif" alt="Supprimer" border="0"></a></td>
</tr>
</table>
</p>
<p style="border-top: 1px #aeaeae solid"><br ></p>
</center>
</body>
</html>
<!-- [Wed, 03 Jun 2009 19:53:36 GMT][0.321297s][6] -->
