/**
 * @file func.js
 * @brief Fonctions diverses.
 * @author Bertrand Usse
 *         Benjamin Debove
 * Copyright (c) 2005 SARL Eriidan
 * @date 2005-06-22
 */

/* require('md5.js'); */

/**
 * @fn chiffrer(src, dst)
 * @brief Chiffrement du mot de passe (hashage MD5).
 * @param src Contr�le source.
 * @param dst Contr�le destination.
 */ 
function chiffrer(src, dst)
{
	var cible = document.getElementById(dst);
	var source = document.getElementById(src);

	cible.value = calcMD5(source.value);
	source.value = "";

	//window.alert(cible.value);

	//return true;
}

/**
 * @brief Basculer l'affichage d'un �l�ment DOM et placer le focus 
 * sur un autre �l�ment
 * @param fn El�ment � basculer.
 * @param fc El�ment � focaliser.
 * @param an Adresse � suivre.
 */
function basculer(fn, fc, an)
{
	var ofn = document.getElementById(fn);
	var ofc = document.getElementById(fc);
	var vs = (ofn.style.display == 'none')? 'block': 'none';

	if (ofn)
		ofn.style.display = vs;
	
	if (ofc.style.display != 'none')
	{
		window.open(an, '_self');
		
		if (ofc)
			ofc.focus();
	}
	
	//window.alert('debug');
}

/**
 * @brief Afficher une fiche normale.
 */
function fiche(url)
{
	window.open(url,'_blank',
		'width=785,height=555,menubar=0,toolbar=0,scrollbars=1,resizable=1');
	//return false;
}

/**
 * @brief Afficher une fiche d�taill�e.
 */
function fiche_detail(url)
{
	window.open(url,'_blank',
		'width=785,height=555,menubar=0,toolbar=0,scrollbars=1,resizable=1');
	//return false;
}


/**
 * @fn previsualiser(bs, ls, pfx , sfx)
 * @brief Previsualisation du th�me courant.
 * @param bs Chemin de base.
 * @param ls Liste.
 * @param pfx pr�fixe.
 * @param sfx suffixe.
 */
function previsualiser2(bs, ls, pfx, sfx)
{
	var ap = document.getElementById('apercu');
	var img = '';	
	
	img = bs+pfx+ls.options[ls.selectedIndex].value+sfx;
	
	if (ap != null)
	{
		ap.src = img;
	}
}

/**
 * @fn previsualiser(bs, ls, pfx , sfx)
 * @brief Previsualisation du th�me courant.
 * @param bs Chemin de base.
 * @param ls Liste.
 * @param pfx pr�fixe.
 * @param sfx suffixe.
 */
function previsualiser(bs, ls,  pfx, sfx)
{
	var ap = document.getElementById('apercu');
	var img = '';	
	
	/*if (charte)
	{
		img = bs+'/charte'+ls.options[ls.selectedIndex].value+'.jpg';
	}
	else
	{
		if (ls.selectedIndex > 0)
			img = bs+ls.options[ls.selectedIndex].value;
		else
			img = ls.options[ls.selectedIndex].value;
	}*/

	
	if (ls.selectedIndex > 0)
		img = bs+pfx+ls.options[ls.selectedIndex].value+sfx;
	else
		img = pfx+ls.options[ls.selectedIndex].value+sfx;

	//window.alert(img);
	
	if (ap != null)
	{
		ap.src = img;
	}
}

/**
 * @fn Cacher_couleur(ls)
 * @brief cacher le choix des couleurs.
 * @param ls Liste.
*/
function Cacher_couleur(ls)
{
	if (ls.options[ls.selectedIndex].value == '0')
		document.getElementById('tab_couleurs').style.display = 'none'
	else 
		document.getElementById('tab_couleurs').style.display = 'block'
}

/**
 * @fn activer(elem)
 * @brief Activation d'un �l�ment dans un formulaire.
 * @param itm El�ment � activer.
 */
function activer(elem)
{
	var cible = document.getElementById(elem);

	if (cible != null)
		cible.focus();

	//return cible.focus();
}

/**
 * @fn rediriger(url, delai)
 * @brief Redirection vers un URL donn�.
 * @param url Adresse cible de la redirection.
 * @param delai D�lai de avant redirection.
 */
function rediriger(url, delai)
{
	var cible = "window.open('"+url+"','_self')";

	window.setTimeout(cible, delai);

	//return window.setTimeout(cible, delai);
}

/**
 * @brief validation d'un formulaire sur le changement d'une chekbox
 */
 function valider(formulaire)
 {
 		formulaire.submit();
 }
 
/**
  * @brief affichage de la map
  */ 
 /*function load(latitude, longitude)
 {
      if (GBrowserIsCompatible()) 
      {
        var map = new GMap2(document.getElementById("map"));
        map.setCenter(new GLatLng('"+latitude+"', '"longitude"'), 13);
        window.setTimeout(function() 
        {
          map.panTo(new GLatLng('"+latitude+"', '"longitude"'));
        }, 1000);
  	  }
 }*/
/***************************/ 
//contenu du script source
/***************************/ 
 var G_INCOMPAT = false;
function GScript(src) 
{
	document.write('<' + 'script src="' + src + '"' +' type="text/javascript"><' + '/script>');
}

function GBrowserIsCompatible()
{
	if (G_INCOMPAT) return false;
	if (!window.RegExp) return false;
	var AGENTS = ["opera","msie","safari","firefox","netscape","mozilla"];
	var agent = navigator.userAgent.toLowerCase();
	for (var i = 0; i < AGENTS.length; i++)
	{
		var agentStr = AGENTS[i];
		if (agent.indexOf(agentStr) != -1) 
		{
			var versionExpr = new RegExp(agentStr + "[ \/]?([0-9]+(\.[0-9]+)?)");
			var version = 0;
			if (versionExpr.exec(agent) != null) 
			{
				version = parseFloat(RegExp.$1);
			}
			if (agentStr == "opera") return version >= 7;
			if (agentStr == "safari") return version >= 125;
			if (agentStr == "msie") return (version >= 5.5 &&agent.indexOf("powerpc") == -1);
			if (agentStr == "netscape") return version > 5;
			if (agentStr == "firefox") return version >= 0.8;
		}
	}
	return !!document.getElementById;
}

function GVerify() {}

function GLoad() 
{
	//modif 
	//if (!GValidateKey("")) 
	//{
	//	G_INCOMPAT = true;
	//	alert("La cl� Google Maps API utilis�e sur ce site Web a �t� enregistr�e pour un autre site. Vous pouvez g�n�rer une nouvelle cl� pour ce site en vous connectant � http://www.google.com/apis/maps/.");
	//	return;
	//}
	GLoadApi(["http://mt0.google.com/mt?n=404&v=w2.37&","http://mt1.google.com/mt?n=404&v=w2.37&","http://mt2.google.com/mt?n=404&v=w2.37&","http://mt3.google.com/mt?n=404&v=w2.37&"], 
	 	["http://kh0.google.com/kh?n=404&v=14&","http://kh1.google.com/kh?n=404&v=14&","http://kh2.google.com/kh?n=404&v=14&","http://kh3.google.com/kh?n=404&v=14&"], 
	 	["http://mt0.google.com/mt?n=404&v=apt.36&","http://mt1.google.com/mt?n=404&v=apt.36&","http://mt2.google.com/mt?n=404&v=apt.36&","http://mt3.google.com/mt?n=404&v=apt.36&"],"","","",false);
	 	if (window.GJsLoaderInit) 
		{
			GJsLoaderInit("http://maps.google.com/mapfiles/maps2.70a.api.js");
		}
}

function GUnload() 
{
	if (window.GUnloadApi) 
	{
		GUnloadApi();
	}
}

var _mFlags = {};
var _mHost = "http://maps.google.com";
var _mUri = "/maps";
var _mDomain = "google.com";
var _mStaticPath = "http://www.google.com/intl/fr_ALL/mapfiles/";
var _mTermsUrl = "http://www.google.com/intl/fr_ALL/help/terms_local.html";
var _mTerms = "Conditions d\'utilisation";
var _mMapMode = "Plan/Carte";
var _mMapModeShort = "Carte";
var _mMapError = "D�sol�s, mais nous ne pouvons pas vous pr�senter de carte de cette r�gion � cette �chelle.<p>Essayez d�effectuer un zoom arri�re.</p>";
var _mSatelliteMode = "Satellite";
var _mSatelliteModeShort = "Sat.";
var _mSatelliteError = "D�sol�s, mais nous ne pouvons pas vous pr�senter de vue a�rienne de cette r�gion � cette �chelle.<p>Essayez d�effectuer un zoom arri�re.</p>";
var _mHybridMode = "Mixte";
var _mHybridModeShort = "Mixte";
var _mTraffic = "";
var _mTrafficShow = "";
var _mTrafficHide = "";
var _mSatelliteToken = "fzwq2hpuK-boJCKCqZ5iBXCpPd_PW29ZukhkMA";
var _mZoomIn = "Zoom avant";
var _mZoomOut = "Zoom arri�re";
var _mZoomSet = "Cliquez pour d�finir le facteur de zoom";
var _mZoomDrag = "Faites glisser le curseur pour zoomer";
var _mPanWest = "D�placer vers la gauche";
var _mPanEast = "D�placer vers la droite";
var _mPanNorth = "D�placer vers le haut";
var _mPanSouth = "D�placer vers le bas";
var _mLastResult = "Revenir au r�sultat initial";
var _mMapCopy = "Donn�es cartographiques &#169;2007";
var _mSatelliteCopy = "Imagerie &#169;2007";
var _mGoogleCopy = "&#169;2007 Google";
var _mKilometers = "km";
var _mMiles = "mi";
var _mMeters = "m";
var _mFeet = "pieds";
var _mPreferMetric = false;
var _mPanelWidth = 20;
var _mTabBasics = "Adresse";
var _mTabDetails = "D�tails";
var _mDecimalPoint = ',';
var _mThousandsSeparator = '&nbsp;';
var _mUsePrintLink = 'Pour restituer le niveau de d�tail visible � l\'�cran, cliquez sur le lien "Imprimer" � c�t� de la carte.';
var _mPrintSorry = '';
var _mMapPrintUrl = 'http://www.google.com/mapprint';
var _mPrint = 'Imprimer';
var _mOverview = 'Aper�u';
var _mStart = 'D�part';
var _mEnd = 'Arriv�e';
var _mStep = '�tape %1$s';
var _mStop = 'Destination %1$s';
var _mHideAllMaps = 'Masquer les cartes';
var _mShowAllMaps = 'Afficher toutes les cartes';
var _mUnHideMaps = 'Afficher les cartes';
var _mShowLargeMap = 'Afficher la carte initiale';
var _mmControlTitle = null;
var _mAutocompleteFrom = 'de';
var _mAutocompleteTo = '�';
var _mAutocompleteNearRe = '^(?:(?:.*?)&#92;s+)(?:(?:in|near|around|close to):?&#92;s+)(.+)$';
var _mSvgEnabled = true;
var _mSvgForced = false;
var _mStreetMapAlt = 'Afficher un plan de ville';
var _mSatelliteMapAlt = 'Afficher des images satellite';
var _mHybridMapAlt = 'Afficher des images satellite avec le nom des rues';
var _mSeeOnGoogleMaps = "Cliquez ici pour afficher cette zone sur Google Maps";
var _mLogInfoWinExp = true;
var _mLogPanZoomClks = false;
var _mLogWizard = true;

function GLoadMapsScript()
{
	if (GBrowserIsCompatible()) 
	{
		GScript("http://maps.google.com/mapfiles/maps2.70a.api.js");
	}
}


GLoadMapsScript();