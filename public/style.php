/**
 * @file style.php
 * @brief Feuille de style dynamique
 * @author Eriidan
 * @date 2018-09-04
 */

@import url('sbase.css');
@import url('slayout.css');

body {
	background-color: #FFFFFF;
	color: #000000;
	vertical-align: middle;
}

td.exterieur {
	background-color: #0C75C6;
	color: #FFFFFF;
}


td.exter_bas {
	background-color: #0C75C6;
	color: #FFFFFF;
}


td.bandeau {
	background-image: url('/public/perso/img-bandeau-36.jpg');
	background-repeat: repeat-x;
	height: 100px;
	padding-left: 10px;
	padding-right: 10px;
}


#bdn_logo {
	text-align: left;
	vertical-align: middle;
	float: left;
	padding: 0;
	margin: 0;
}

#bdn_titre{
	font-family: "Verdana", sans-serif;
	font-style: italic;
	font-size: 26pt;
	color: #ffffff;
	padding-left: 10px;
	float: right;
}

td.interieur {
	background-color: #FFFFFF;
	color: #0C75C6;
}

td.menu {
	background-color: #0C75C6;
	color: #FFFFFF;
}

a.menu:link {
	color: #FFFFFF;
	font-weight: bold;
}

a.menu:visited {
	color: #FFFFFF;
	font-weight: bold;
}

td.panneau {
	background-color: #FFFFFF;
	color: #000000;
}

th.panneau_rubrique {
	background-color: #FFFFFF;
	color: #000000;
	border-top: 1px #0C75C6 solid;
	border-left: 1px #0C75C6 solid;
	border-right: 1px #0C75C6 solid;
	border-bottom: 1px #0C75C6 solid;
	font-weight: bold;
	text-align: left;
}

td.panneau_elem {
	background-color: #FFFFFF;
	color: #000000;
	border-bottom: 1px #000000 solid;
	border-left: 1px #000000 solid;
	border-right: 1px #000000 solid;
	font-weight: bold;
	text-align: left;
	padding-left: 4px;
}

a.panneau:link {
	color: #000000;
}

span.panneau {
	color: #000000;
	font-weight: bold;
}

a.panneau:visited {
	color: #000000;
}

td.detail {
	background-color: #FFFFFF;
	color: #0C75C6;
}

td.groupe {
	padding-top: 2px;
	padding-bottom: 2px;
	padding-left: 4px;
	padding-right: 4px;
	background-color: #0C75C6;
	color: #FFFFFF;
	border: 1px #FFFFFF solid;
	font-weight: bold;
	text-align: left;
}

div.groupe {
	background-color: #FFFFFF;
	color: #0C75C6;
	border: 1px #000 solid;
	font-weight: bold;
	padding: 2px;
}

#recherche_rapide {
	background-color: #FFFFFF;
	color: #0C75C6;
	text-align: right;
	vertical-align: middle;
}

.calendrier_cadre {
	background-color: #fff;
	border-bottom: 1px #000 solid;
	border-right: 1px #000 solid;
	color: #000;
	width: 158px;}

.calendrier_top {
	background-color: #fff;
	color: #000;
}

.calendrier_jour_entete {
	background-color: #fff;
	color: #000;
}

.calendrier_jour_vide {
	background-color: #fff;
	color: #fff;
}

.calendrier_jour_avant_courant {
	background-color: #fff;
	color: #000;
	border-bottom: 1px #000 solid;
	border-right: 1px #000 solid;
}

.calendrier_jour_apres_courant {
	background-color: #fff;
	color: #000;
	border-bottom: 1px #000 solid;
	border-right: 1px #000 solid;
}

.calendrier_jour_courant {
	background-color: #0C75C6;
	color: #FFFFFF;
	font-weight: bold;
}

.calendrier_jour_courant_evenement {
	background-color: #fff;
	color: #f00;
	font-weight: bold;
}

.calendrier_jour_evenement {
	background-color: #f00;
	color: #fff;
}

