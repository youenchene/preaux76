function LoadMenus()
{
	window.menu_52 = new Menu("root",189,20,"sans-serif",10,
		"#FFFFFF","#0C75C6","#0C75C6","#FFFFFF",
		"left","middle",5,2,1000,-5,7,true,true,true,5,true,true);

	menu_52.addMenuItem("Accueil","location='?code=accueil'");
	menu_52.addMenuItem("Bulletin Municipal","location='?code=pub_bulletin'");
	menu_52.addMenuItem("CMJ","location='?code=conseil-municipal-des-jeu'");
	menu_52.addMenuItem("Conseil Municipal et Elus","location='?code=conseil-municipal'");
	menu_52.addMenuItem("P�riscolaire","location='?code=test-am-activites-peri-sc'");
	menu_52.addMenuItem("Ecoles","location='?code=les-ecoles'");
	menu_52.addMenuItem("Horaires et Infos Pratiques","location='?code=services-municipaux'");
	menu_52.addMenuItem("Petite enfance","location='?code=petite-enfance'");
	menu_52.addMenuItem("Biblioth�que","location='?code=la-bibliotheque'");
	menu_52.addMenuItem("Historique","location='?code=test-1'");
	menu_52.addMenuItem("PLU de Pr�aux","location='?code=page229'");
	menu_52.addMenuItem("Environnement, D�chets","location='?code=dechets--collecte,-recycl'");
	menu_52.addMenuItem("Conseils domestiques","location='?code=travaux'");
	menu_52.addMenuItem("Plans de Pr�aux","location='?code=plan-de-preaux'");
	menu_52.addMenuItem("Randonn�es P�destres","location='?code=randonnees-pedestres'");
	menu_52.addMenuItem("Communaut� de Communes","location='?code=page240'");
	menu_52.addMenuItem("SIAEPA du Crevon","location='?code=page236'");
	menu_52.addMenuItem("Pays entre Seine et Bray","location='?code=le-pays-enntre-seine-et-b'");
	menu_52.addMenuItem("SDE 76","location='?code=syndicat-departement-elec'");

	menu_52.hideOnMouseOut=true;
	menu_52.menuBorder=1;
	menu_52.menuLiteBgColor='#ffffff';
	menu_52.menuBorderBgColor='#555555';
	menu_52.bgColor='#000000';

	window.menu_54 = new Menu("root",343,20,"sans-serif",10,
		"#FFFFFF","#0C75C6","#0C75C6","#FFFFFF",
		"left","middle",5,2,1000,-5,7,true,true,true,5,true,true);

	menu_54.addMenuItem("Registres d'accessibilit�","location='?code=registre-d-accessibilite'");
	menu_54.addMenuItem("D�marches administratives","location='?code=pub_demarche'");
	menu_54.addMenuItem("Infos publiques","location='?code=pub_comarquage'");
	menu_54.addMenuItem("Horaires r�glement�s","location='?code=horaires-a-respecter'");
	menu_54.addMenuItem("Nouveau site services de L'Etat en Seine Maritime","location='?code=nouveau-site-services-de-'");

	menu_54.hideOnMouseOut=true;
	menu_54.menuBorder=1;
	menu_54.menuLiteBgColor='#ffffff';
	menu_54.menuBorderBgColor='#555555';
	menu_54.bgColor='#000000';

	window.menu_53 = new Menu("root",175,20,"sans-serif",10,
		"#FFFFFF","#0C75C6","#0C75C6","#FFFFFF",
		"left","middle",5,2,1000,-5,7,true,true,true,5,true,true);

	menu_53.addMenuItem("Entreprises","location='?code=pub_hebergement'");
	menu_53.addMenuItem("Commerces","location='?code=pub_association'");
	menu_53.addMenuItem("Services","location='?code=services'");
	menu_53.addMenuItem("Contact Essaim d'abeilles","location='?code=apiculteurs'");
	menu_53.addMenuItem("Sant�","location='?code=sante'");
	menu_53.addMenuItem("Paroisse","location='?code=paroisse'");

	menu_53.hideOnMouseOut=true;
	menu_53.menuBorder=1;
	menu_53.menuLiteBgColor='#ffffff';
	menu_53.menuBorderBgColor='#555555';
	menu_53.bgColor='#000000';

	window.menu_57 = new Menu("root",96,20,"sans-serif",10,
		"#FFFFFF","#0C75C6","#0C75C6","#FFFFFF",
		"left","middle",5,2,1000,-5,7,true,true,true,5,true,true);

	menu_57.addMenuItem("Associations","location='?code=pub_commerce'");

	menu_57.hideOnMouseOut=true;
	menu_57.menuBorder=1;
	menu_57.menuLiteBgColor='#ffffff';
	menu_57.menuBorderBgColor='#555555';
	menu_57.bgColor='#000000';

	window.menu_55 = new Menu("root",144,20,"sans-serif",10,
		"#FFFFFF","#0C75C6","#0C75C6","#FFFFFF",
		"left","middle",5,2,1000,-5,7,true,true,true,5,true,true);

	menu_55.addMenuItem("H�bergement","location='?code=pub_entreprise'");
	menu_55.addMenuItem("Id�es de sorties","location='?code=idees-de-sorties-2'");
	menu_55.addMenuItem("Terroir, tradition","location='?code=page196'");

	menu_55.hideOnMouseOut=true;
	menu_55.menuBorder=1;
	menu_55.menuLiteBgColor='#ffffff';
	menu_55.menuBorderBgColor='#555555';
	menu_55.bgColor='#000000';

	window.menu_56 = new Menu("root",88,20,"sans-serif",10,
		"#FFFFFF","#0C75C6","#0C75C6","#FFFFFF",
		"left","middle",5,2,1000,-5,7,true,true,true,5,true,true);

	menu_56.addMenuItem("Nous �crire","location='?code=pub_contact'");

	menu_56.hideOnMouseOut=true;
	menu_56.menuBorder=1;
	menu_56.menuLiteBgColor='#ffffff';
	menu_56.menuBorderBgColor='#555555';
	menu_56.bgColor='#000000';

	menu_56.writeMenus();
} // LoadMenus()
